# merlin-framework-example

At Surfboard, engineers have been working on AI/ML projects for camera and users. We have recently cooperated with NVIDIA to bring our AI/ML technologis to the next level. Hence, we decide to release guidelines to setup Merlin Framework developed NVIDIA user Deep Learning tasks. 

#### Dependencies
* [NVTabular](https://github.com/NVIDIA/NVTabular) - An optimized and GPU-based Feature Engineering package
* [HugeCTR](https://github.com/NVIDIA/HugeCTR) - A Deep Learning package to train model swith ease that you just need to modify JSON configuration files
* [TensorRT](https://github.com/NVIDIA/TensorRT) - A Deep Learning package for model deployment and inference with least latency

#### Hardware Requirements
* Merlin is an entirely GPU-based Deep Learning. Hence, to fully utilize Merlin, we recommend you to install Merlin on your local machine equipped with at least one GPU or a cloud GPU instance (e.g. AWS SageMaker ml.p2xlarge)
* Here, we show steps to install Merlin on AWS SageMaker ml.p3.2xlarge instance that using one V100 GPU

#### Steps to install and run:
* <strong> Install NVTabular </strong>
```
---- Install by Docker
docker pull nvcr.io/nvidia/nvtabular:0.1
```

* <strong> Run NVTabular </strong>
```
docker run --runtime=nvidia --rm -it -p 8888:8888 -p 8797:8787 -p 8796:8786 --ipc=host --cap-add SYS_PTRACE nvcr.io/nvidia/nvtabular:0.1 /bin/bash
source activate rapids

---- Run by JupyterNotebook (not recommended on AWS)
jupyter-lab --allow-root --ip='0.0.0.0' --NotebookApp.token='<password>'

---- Embed NVTabular into Python scripts
import nvtabular
```

Attempts to install by "conda" poses difficulties due to incompatability between AWS SageMaker fixed configurations and configurations required by NVTabular

* <strong> Install HugeCTR </strong>
```
---- Install Nvidia Docker 
distribution=$(. /etc/os-release;echo $ID$VERSION_ID)
curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.repo | sudo tee /etc/yum.repos.d/nvidia-docker.repo

sudo yum install -y nvidia-container-toolkit
sudo service docker restart
---- Install HugeCTR
git clone https://github.com/NVIDIA/HugeCTR.git
docker build -t hugectr:devel .
```

* <strong> Run HugeCTR </strong>
```
---- Run HugeCTR docker 
docker run --runtime=nvidia --rm -it -u $(id -u):$(id -g) -v $(pwd):/hugectr -w /hugectr hugectr:devel bash
mkdir -p build
cd build
cmake -DCMAKE_BUILD_TYPE=Release -DSM=70 .. #using V100
make -j
```
If command "make -j" fails, execute "sudo service docker restart" and repeat the Run HugeCTR docker step again.